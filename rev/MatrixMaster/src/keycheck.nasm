%include "vm.inc"

; flag = GLUG{EscaP3_7He_m@TRIx}
    
;k[0] = G;k[1] = L;k[2] = U;k[3] = G

vmovi vr11, 0x1a1b1c1d
vmovi vr10, 0x5d4e505a
vxor vr10, vr11
vxor vr0, vr10  

;k[4] = {;k[5] = E;k[6] = s;k[7] = c

vmovi vr11, 0x2a2b2c2d
vmovi vr10, 0x49586956
vxor vr10, vr11
vxor vr1, vr10

;k[8] = a;k[9] = P;k[10] = 3;k[11] = _

vmovi vr11, 0x3a3b3c3d
vmovi vr10, 0x65086c5c
vxor vr10, vr11
vxor vr2, vr10

;k[12] = 7;k[13] = H;k[14] = e;k[15] = _

vmovi vr11, 0x4a4b4c4d
vmovi vr10, 0x152e047a
vxor vr10, vr11
vxor vr3, vr10

;k[16] = m;k[17] = @;k[18] = T;k[19] = R

vmovi vr11, 0x5a5b5c5d
vmovi vr10, 0x0080f1c30
vxor vr10, vr11
vxor vr4, vr10

;k[20] = I;k[21] = x;k[22] = };k[23] == 0

vmovi vr11, 0x6a6b6c6d
vmovi vr10, 0x6a161424
vxor vr10, vr11
vxor vr5, vr10

vor vr0, vr1
vor vr0, vr2
vor vr0, vr3
vor vr0, vr4
vor vr0, vr5



vend